# Industria 4.0
## Es una hipotética cuarta mega etapa de la evolución técnico-económica de la humanidad, contando a partir de la Primera Revolución Industrial
```plantuml
@startmindmap
*[#Gray]  **Industria 4.0**
	*_ REALIZARAN
		*[#LightBlue] **Integracion de Tics**
			*[#OrangeRed] **Fabrica de Automoviles**
			*[#OrangeRed] **Entrega de Productos**
			*[#OrangeRed] **Manejo de productos**
			*[#OrangeRed] **Efectos**
				*_ QUE APARECEREAN
					*[#LightGreen] **Reduccion de empleo**	
					*[#LightGreen] **Aparicion de**  \n **nuevas profesiones**
	*_ EMPRESAS DE TICS
		*[#BlueViolet] **Electrodomesticos** \n **modernos**
		*[#BlueViolet] **Industrias de automovil**
		*[#BlueViolet] **Efecto**
			*_ EXISTIRA
				*[#YellowGreen] **Menos diferencias** \n **entre industrias** 
	*_ NUEVOS PARADIGMAS Y TECNOLOGIAS
		*[#LightPink] **Velocidad en los negocios**
		*[#LightPink] **Negocios basados** \n **en plataforma**
			*_ COMO SON:
				*[#SkyBlue] **FANG**
				*[#SkyBlue] **BAT**
		*[#LightPink] **Internet de la cosas**
			*_ COMO SON:
				*[#Green] **Inteligencia ** \n **artificial**
				*[#Green] **BigData ** 
	*_ NUEVAS CULTURAS DIGITALES
		*[#Yellow] **Phono Sapiens**
		*[#Yellow] **Youtubers**
		*[#Yellow] **E-Sports**		
@endmindmap
```mindmap
```
# Mexico y la industra 4.0
## Las bases    para consolidar el desarrollo de la manufactura en México son la colaboración entre iniciativa privada, gobierno y universidades, así como la inversión en investigación y capacitación.
-Manuel Nieblas, Lider de Productos industriales y Manufactura.

```plantuml
@startmindmap
*[#Yellow]  **Mexico y la Industria 4.0**
	*_ CUARTA REVOLUCION
		*[#LightBlue] **Principales actores de ** \n **ecosistemas emprendedores**
			*_ FONDOS DE INVERSION
			*_ GOBIERO
				*[#LightYellow] **Perfiles Laborales**
				*[#LightYellow] **Reforma educativa**
			*_ EMPRENDEDORES
				*[#LightCyan] **Reduccion de costos**
				*[#LightCyan] **Empresas inovadoras**
		*[#LightBlue] **Introduccion de ** \n **tecnologias digitales**
			*_ BENEFICIOS
				*[#LightGreen] **Mayor nivel de empleo** \n **= mayor valor**            
			*_ INCONVENIENTES
				*[#LightCoral] **Perdida de Empleo**
				*[#LightCoral] **Seguridad**              
			*_ FACILITA EL DIA A DIA
		*[#LightBlue] **Mexico**
			*_ LO QUE QUIERE
				*[#OrangeRed] **Visibilidad** 
				*[#OrangeRed] **Transparencia**
				*[#OrangeRed] **Prediccion**
				*[#OrangeRed] **Adaptacion**           
			*_ LO QUE TIENE
				*[#LightGreen] **Procesos Automatizados**
				*[#LightGreen] **PLC**
			*_ LO QUE NECESITA
				*[#LightGray] **Adaptacion a los cambios**
				*[#LightGray] **Infraestructuras**
				*[#LightGray] **Centros de trabajo**
				*[#LightGray] **Equipo de trabajo**
				*[#LightGray] **Capital humano**
@endmindmap
```mindmap
```
### Grijalva Cruz Keila Jocabed
### Fundamentos de Ingenieria de Software.

